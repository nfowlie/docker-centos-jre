# Supported Tags
* latest ([*Dockerfile*](https://bitbucket.org/nfowlie/docker-centos-jre/src/83b8dfa6c1c37947d203ec1ea0f48739cdd54e8e/Dockerfile?at=8u60))
* 1.7.0_latest ([*Dockerfile*](https://bitbucket.org/nfowlie/docker-centos-jre/src/cdd349289d2baf597b3b79be2e516a41337e4ac8/Dockerfile?at=7u80))
* 1.8.0_latest ([*Dockerfile*](https://bitbucket.org/nfowlie/docker-centos-jre/src/83b8dfa6c1c37947d203ec1ea0f48739cdd54e8e/Dockerfile?at=8u60))
* Any JRE 7/8 version, in the format 1.<version>.0_<build> ie: 1.8.0_40, 1.7.0_21.

# What's in this image?
This image contains the latest version of CentOS, running the latest version of the Oracle JRE (`nfowlie/centos-jre:latest`).

All versions of the Oracle JRE 7 & 8 are supported, refer to https://hub.docker.com/r/nathonfowlie/centos-jre/tags/ for the complete list of available tags.

# Supported Docker Versions
This has been tested with Docker version 1.7.0 and higher. Mileage may vary with earlier versions.

# Issues
If you have any issues with this image, please [raise a bug](https://bitbucket.org/nfowlie/docker-centos-jre/issues/new)